package com.gemframework.modules.prekit.auth.entity.request;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.validation.constraints.NotBlank;

@Slf4j
@Data
public class AppidAndSecretAuthRequest extends AuthRequest {

    @NotBlank(message = "appid不能为空")
    private String appid;
    @NotBlank(message = "appSecret不能为空")
    private String appSecret;
}
