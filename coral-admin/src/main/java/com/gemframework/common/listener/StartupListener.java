/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.listener;
import com.gemframework.common.utils.GemRedisUtils;
import com.gemframework.model.entity.po.Dictionary;
import com.gemframework.service.DictionaryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Title: StartListener
 * @Package: com.gemframework.common.listener
 * @Date: 2020-06-26 14:54:36
 * @Version: v1.0
 * @Description: 监听SpringBoot启动
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
@Component
public class StartupListener implements ApplicationListener<ApplicationReadyEvent> {

    //授权版本功能
    //@Autowired
    //JobsService jobsService;
    @Autowired
    GemRedisUtils gemRedisUtils;
    @Autowired
    DictionaryService dictionaryService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        //==================执行一系列初始化操作========================
        //jobsService.initJobs(); 授权版本功能
        //将常用的系统常量同步到缓存
        List<Dictionary> dictionaries = dictionaryService.list();
        if(dictionaries!=null && dictionaries.size()>0){
            for(Dictionary dictionary:dictionaries){
                gemRedisUtils.set(dictionary.getKeyName(),dictionary);
            }
        }
    }
}